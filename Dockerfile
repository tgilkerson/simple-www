FROM bitnami/nginx:1.22

COPY nginx_server_block.conf /opt/bitnami/nginx/conf/server_blocks/nginx_server_block.conf
WORKDIR /app

COPY  . .
